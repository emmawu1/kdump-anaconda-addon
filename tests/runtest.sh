#!/bin/sh
TMPDIR="/var/tmp"
kdump_anaconda_addon_env=""
kdump_anaconda_addon_path=""

# We do this because there is no pylint in RHEL
kdump_anaconda_addon_env=$(mktemp -p "$TMPDIR/" -d -t kdump-anaconda-addon-test.XXXXXX)
# Need to use anaconda package in RHEL, so let virtualenv access system site packages
virtualenv $kdump_anaconda_addon_env --system-site-packages
# Start it and install pylint
source $kdump_anaconda_addon_env/bin/activate
pip install --ignore-installed pylint

# Run test
pushd source
make test
popd
